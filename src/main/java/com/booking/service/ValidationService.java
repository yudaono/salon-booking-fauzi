package com.booking.service;

import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    private static Scanner input = new Scanner(System.in);

    public static String validateInput(String regex){
        String value;
        System.out.println("Masukan Input :");
        do {
            value = input.nextLine();
            if (!value.matches(regex)){
                System.out.println("Input tidak sesuai, masukan kembali");
            }
        } while (!value.matches(regex));
        return value;
    }

    public static Person validatePersonId(List<Person> personList){
        String value;
        Person selectedPerson = null;
        boolean isTrue = false;
        do {
            value = input.nextLine();
            for(Person person : personList){
                if(value.equals(person.getId())){
                    isTrue = true;
                    selectedPerson = person;
                }
            }
            if (!isTrue) {
                System.out.println("ID yang dicari tidak tersedia");
            }
        } while (!isTrue);
        return selectedPerson;
    }

    public static Service validateServiceId(List<Service> serviceList){
        String value;
        Service selectedService = null;
        boolean isTrue = false;
        do {
            value = input.nextLine();
            for(Service service : serviceList){
                if(value.equals(service.getServiceId())){
                    isTrue = true;
                    selectedService = service;
                }
            }
            if (!isTrue) {
                System.out.println("ID yang dicari tidak tersedia");
            }
        } while (!isTrue);
        return selectedService;
    }

    public static Reservation validateReservationId(List<Reservation> reservationList){
        String value;
        Reservation selectedReservation = null;
        boolean isTrue = false;
        do {
            value = input.nextLine();
            for(Reservation reservation : reservationList){
                if(value.equals(reservation.getReservationId())){
                    isTrue = true;
                    selectedReservation = reservation;
                }
            }
            if (!isTrue) {
                System.out.println("ID yang dicari tidak tersedia");
            }
        } while (!isTrue);
        return selectedReservation;
    }
}
