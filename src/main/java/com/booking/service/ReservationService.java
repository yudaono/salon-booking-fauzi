package com.booking.service;

import java.util.ArrayList;
import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    public static final String[] LIST_MEMBERSHIP_STATUS = {"none", "Silver", "Gold"};
    public static final double[] LSIT_MEMBERSHIP_DISCOUNT = {0.0, 0.05, 0.1};
    private static int id = 0;
    public static void createReservation(List<Person> personList, List<Service> serviceList, List<Reservation> reservationList){
        PrintService.showAllCustomer(personList);
        System.out.println("Masukan Costumer ID :");
        Person costumer = ValidationService.validatePersonId(personList);

        PrintService.showAllEmployee(personList);
        System.out.println("Masukan Employee ID :");
        Person employee = ValidationService.validatePersonId(personList);

        String anotherService;
        List<Service> services = new ArrayList<>();
        do {
            PrintService.showService(serviceList);
            System.out.println("Masukan Service ID");
            Service service = ValidationService.validateServiceId(serviceList);
            services.add(service);
            System.out.println("Ingin pilih service yang lain (Y/T)?");
            anotherService = ValidationService.validateInput("^[Y|T]$");
        } while (anotherService.equals("Y"));

        id++;
        String reservationId = String.format("Rsv-%02d", id);
        System.out.println(reservationId);
        
        Reservation reservation = new Reservation(reservationId, (Customer) costumer, (Employee) employee, services, "In Progress");

        double servicePrice = calculateServicePrice(services);
        double servicePriceDisc = calculateDiscountPrice(servicePrice, reservation.getCustomer());
        reservation.setReservationPrice(servicePriceDisc);

        reservationList.add(reservation);
        
        System.out.println("Booking Berhasil!");
        System.out.printf("Total Biaya Booking: Rp. %d\n", (int)servicePriceDisc);
    }

    public static void editReservationWorkstage(List<Reservation> reservationList){
        PrintService.showRecentReservation(reservationList);
        System.out.println("Masukan Reservation ID :");
        Reservation reservation = ValidationService.validateReservationId(reservationList);
        System.out.println("Selesaikan Reservasi :");
        String status = ValidationService.validateInput("^[a-zA-Z]*$");
        reservation.setWorkstage(status);

        System.out.printf("Reservasi dengan id %s sudah %s" , reservation.getReservationId(), reservation.getWorkstage());
    }

    public static double calculateHistoryRerservationPrice(List<Reservation> reservationList){
        double result = 0;
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")){
                result = result + reservation.getReservationPrice();
            }
        }
        return result;
    }

    public static double calculateServicePrice(List<Service> serviceList) {
        double result = 0;
        for (Service service : serviceList) {
            result = result + service.getPrice();
        }
        return result;
    }

    public static double calculateDiscountPrice(double price, Customer person){
        double result = price;
        for (int index = 0; index < LIST_MEMBERSHIP_STATUS.length; index++) {
            if (LIST_MEMBERSHIP_STATUS[index].equals(person.getMember().getMembershipName())){
                double discount = LSIT_MEMBERSHIP_DISCOUNT[index] * price;
                result = price-discount;
            }
        }
        return result;
    } 

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
